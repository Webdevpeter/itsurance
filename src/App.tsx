import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

import RecordsTable from './components/RecordsTable';

import mergeCategories from './helpers/mergeCategories';
import mergeRecordsWithRecordsData from './helpers/mergeRecordsWithRecordsData';
import splitRecordsDataAndRecords from './helpers/splitRecordsDataAndRecords';
import splitRecordsDataToCategories, { RecordsDataCategory } from './helpers/splitRecordsDataToCategories';

import { MergedRecordData } from './mocks/recordsData';
import { getRecords, getRecordsData } from './mocks/requests';

import { palette } from './palette';
import { NO_CATEGORY_STRING } from './constants';

const AppWrapper = styled.div`
    max-width: 800px;
    margin: auto;
    padding: 20px 0;
`;

const TableWrapper = styled.div`
    padding: 0 0 35px;
`;

const CategoryHeader = styled.h2`
    margin-top: 0;
`;

const CategoryName = styled.span`
    color: ${palette.blue[0]};
    font-weigth: 500;
`;

const Button = styled.button`
    background: trnasparent;
    border: 2px solid ${palette.blue[1]};
    border-color: ${palette.blue[1]};
    border-radius: 2px;
    color: ${palette.blue[1]};
    padding: 10px 15px;
    cursor: pointer;
    transition: background .2s ease-in-out, color .2s ease-in-out, border-color .2s ease-in-out;

    &:hover {
        color: #fff;
        border-color: ${palette.blue[3]};
        background: ${palette.blue[3]};
    }
`;

function App() {
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [categories, setCategories] = useState<RecordsDataCategory>({});

    useEffect(() => {
        Promise.all([getRecords(), getRecordsData()]).then(values => {
            const [
                records,
                recordsData
            ] = values;

            setCategories(splitRecordsDataToCategories(mergeRecordsWithRecordsData(records.response.data.items, recordsData.response.data.items)));
            setIsLoading(false);
        })
    }, []);

    const onRowClick = (record: MergedRecordData) => {
        const categoryName = record.category || NO_CATEGORY_STRING;
        setCategories({
            ...categories,
            [categoryName]: categories[categoryName].map(categoryRecord => {
                return categoryRecord.data_id !== record.data_id ? categoryRecord : {
                    ...record,
                    is_favorite: record.is_favorite === '0' ? '1' : '0'
                }
            })
        })
    }

    return (
        <AppWrapper>
            <h1>Podział według kategorii</h1>
            {!isLoading && Object.entries(categories).map(category => {
                const [categoryName, categoryRecords] = category;

                return <TableWrapper key={`category-${categoryName}`}>
                    <CategoryHeader>Kategoria: <CategoryName>"{categoryName}"</CategoryName></CategoryHeader>
                    <RecordsTable records={categoryRecords} onRowClick={onRowClick} />
                </TableWrapper>
            })}
            <Button onClick={() => {console.log(splitRecordsDataAndRecords(mergeCategories(categories)))}}>Zatwierdź</Button>
        </AppWrapper>
    );
}

export default App;
