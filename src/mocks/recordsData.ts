const recordsData: RecordsData = {
    response: {
        data: {
            items: [
                {
                    is_invalid: '0',
                    data_id: 'ascea',
                    name: 'Andrzej Kowalski',
                    category: 'personal',
                    tags: 'tag1.tag2.tag3.tag4',
                    is_favorite: '0'
                },
                {
                    is_invalid: '0',
                    data_id: 'kdlsd',
                    name: 'Jan Andrzejewski',
                    category: 'personal',
                    tags: 'tag1.tag3.tag4',
                    is_favorite: '1'
                },
                {
                    is_invalid: '0',
                    data_id: 'udfds',
                    name: 'Marzena Lis',
                    category: 'personal',
                    tags: '',
                    is_favorite: '0'
                },
                {
                    is_invalid: '1',
                    data_id: 'zxcbc',
                    name: 'Patrycja Kozłowska',
                    category: 'work',
                    tags: 'tag1.tag4',
                    is_favorite: '0'
                },
                {
                    is_invalid: '0',
                    data_id: 'mnsds',
                    name: 'Janusz Piotrowski',
                    category: 'work',
                    tags: 'tag3.tag4',
                    is_favorite: '0'
                },
                {
                    is_invalid: '0',
                    data_id: 'rtrvx',
                    name: 'Piotr Skibiński',
                    category: 'work',
                    tags: 'tag4',
                    is_favorite: '0'
                },
                {
                    is_invalid: '0',
                    data_id: 'jdfds',
                    name: 'Karolina Skibińska',
                    category: '',
                    tags: '',
                    is_favorite: '0'
                },
                {
                    is_invalid: '0',
                    data_id: 'pppas',
                    name: 'Anna Królik',
                    category: '',
                    tags: 'tag1.tag2',
                    is_favorite: '1'
                },
                {
                    is_invalid: '0',
                    data_id: 'eekdv',
                    name: 'Beata Czerwińska',
                    category: '',
                    tags: 'tag1.tag2.tag3.tag4',
                    is_favorite: '0'
                }
            ]
        }
    }
}

export interface RecordsData {
    response: {
        data: {
            items: RecordData[];
        }
    }
}

export interface RecordData {
    is_invalid: IsInvalid;
    data_id: string;
    name: string;
    category: string;
    tags: string;
    is_favorite: IsFavourite;
}

export interface MergedRecordData extends RecordData {
    phoneNumber: string;
}

export type IsInvalid = '0' | '1';
export type IsFavourite = '0' | '1';

export default recordsData;