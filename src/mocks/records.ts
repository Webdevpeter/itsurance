const records: Records = {
  response: {
    data: {
      items: [ // klucz to id, wartość to phoneNumber
        { ascea: '600654654' },
        { kdlsd: '605235444' },
        { udfds: '502522145' },
        { zxcbc: '515458798' },
        { mnsds: '532457789' },
        { rtrvx: '545789222' },
        { jdfds: '665322999' },
        { pppas: '699444555' },
        { eekdv: '' },
      ]
    }
  }
}

export interface Records {
    response: {
        data: {
            items: Record[];
        }
    }
}

export interface Record {
    [key: string]: string;
}

export default records;