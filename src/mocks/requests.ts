import records, { Records } from "./records";
import recordsData, { RecordsData } from "./recordsData";

const getRecords = (): Promise<Records> => {
    return new Promise((resolve => {
        resolve(records)
    }));
}

const getRecordsData = (): Promise<RecordsData> => {
    return new Promise((resolve => {
        resolve(recordsData)
    }));
}

export { getRecords, getRecordsData };