export const palette = {
    blue: ['#7A85AD', '#515F90', '#303E73', '#172557', '#07123A'],
    red: ['#FFAAAA', '#D46A6A', '#AA3939', '#801515', '#550000']
}