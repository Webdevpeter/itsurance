import { MergedRecordData } from "../../mocks/recordsData";

export interface RecordsTableProps {
    records: MergedRecordData[];
    onRowClick: (record: MergedRecordData) => void;
}

export type SortBy = 'name' | 'phoneNumber' | 'tags' | 'favourite';

export interface SortState {
    sortBy: SortBy;
    isAscending: boolean;
}