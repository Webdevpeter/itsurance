import React, { useState } from 'react';
import styled from 'styled-components';

import sortRecords from '../../helpers/sortRecords';
import { palette } from '../../palette';
import SortIndicator from '../SortIndicator';
import { RecordsTableProps, SortBy, SortState } from './types';

const Table = styled.table`
    width: 100%;
    table-layout: fixed;
`;

const Th = styled.th`
    background: ${palette.blue[2]};
    color: #fff;
    padding: 10px;
    transition: box-shadow .2s ease-in-out;
    box-shadow: inset 0px 0px 0px 0px;
    
    &:hover {
        box-shadow: inset 0px 0px 30px -10px;
        cursor: pointer;
    }
`;

interface TrProps {
    isInvalid?: boolean;
}

const Tr = styled.tr<TrProps>`
    background: ${({isInvalid}) => isInvalid ? palette.red[1] : palette.blue[0]};
    transition: box-shadow .2s ease-in-out;
    box-shadow: inset 0px 0px 0px 0px #fff;

    &:hover {
        box-shadow: inset 0px 0px 30px -10px #fff;
        cursor: pointer; 
    }
`;

const Td = styled.td`
    padding: 10px;

    &:last-child {
        text-align: center;
    }
`;

interface ThContentProps {
    justify?: 'flex-start' | 'center' | 'flex-end';
}

const ThContent = styled.div<ThContentProps>`
    display: flex;
    align-items: center;
    ${({justify}) => justify ? `justify-content: ${justify};` : ''};
`;

const Input = styled.input`
    -webkit-appearance: none;
    background-color: #fafafa;
    border: 1px solid #cacece;
    box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05);
    padding: 9px;
    border-radius: 3px;
    display: inline-block;
    position: relative;
    cursor: pointer;

    &:active, &:checked:active {
        box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px 1px 3px rgba(0,0,0,0.1);
    }

    &:checked {
        background-color: #e9ecee;
        border: 1px solid #adb8c0;
        box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05), inset 15px 10px -12px rgba(255,255,255,0.1);
        color: #99a1a7;
    }

    &:checked:after {
        content: '\\2714';
        font-size: 14px;
        position: absolute;
        top: 0px;
        left: 3px;
        color: ${palette.blue[2]};
    }
`;

const onThClick = (sortBy: SortBy, sortState: SortState, callback: React.Dispatch<React.SetStateAction<SortState>>) => {
    callback({
        sortBy: sortBy,
        isAscending: sortState.sortBy === sortBy ? !sortState.isAscending : true
    })
}

const RecordsTable = (props: RecordsTableProps) => {
    const [sortState, setSortState] = useState<SortState>({sortBy: 'name', isAscending: true});

    const { records, onRowClick } = props;
    const sortedRecords = sortRecords(records, sortState);

    return <Table>
        <thead>
            <tr>
                <Th onClick={() => onThClick('name', sortState, setSortState)}>
                    <ThContent>
                        Imię&nbsp;<SortIndicator isDisplayed={sortState.sortBy === 'name'} isAscending={sortState.isAscending} />
                    </ThContent>
                </Th>
                <Th onClick={() => {onThClick('phoneNumber', sortState, setSortState)}}>
                    <ThContent>
                        Numer telefonu&nbsp;<SortIndicator isDisplayed={sortState.sortBy === 'phoneNumber'} isAscending={sortState.isAscending} />
                    </ThContent>
                </Th>
                <Th onClick={() => {onThClick('tags', sortState, setSortState)}}>
                    <ThContent>
                        Tagi&nbsp;<SortIndicator isDisplayed={sortState.sortBy === 'tags'} isAscending={sortState.isAscending} />
                    </ThContent>
                </Th>
                <Th onClick={() => {onThClick('favourite', sortState, setSortState)}}>
                    <ThContent justify={'center'}>
                        Ulubione&nbsp;<SortIndicator isDisplayed={sortState.sortBy === 'favourite'} isAscending={sortState.isAscending} />
                    </ThContent>
                </Th>
            </tr>
        </thead>
        <tbody>
            {sortedRecords.map(record => (
                <Tr isInvalid={record.is_invalid === '1' || !record.phoneNumber} onClick={() => {onRowClick(record)}} key={`row-${record.data_id}`}>
                    <Td>{record.name}</Td>
                    <Td>{record.phoneNumber}</Td>
                    <Td>{record.tags.replace(/\./g, ', ')}</Td>
                    <Td><Input type="checkbox" onChange={() => {}} checked={record.is_favorite === '1' ? true : false} /></Td>
                </Tr>)
            )}
        </tbody>
    </Table>
}

export default RecordsTable;