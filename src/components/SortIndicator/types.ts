export interface SortIndicatorProps {
    isAscending: boolean;
    isDisplayed?: boolean;
}