import React from 'react';
import { AiFillCaretDown, AiFillCaretUp } from 'react-icons/ai';

import { SortIndicatorProps } from './types';

const SortIndicator = (props: SortIndicatorProps) => {
    const { isAscending, isDisplayed } = props;
    if (!isDisplayed) {
        return <></>
    }

    return isAscending ? <AiFillCaretUp /> : <AiFillCaretDown />;
}

export default SortIndicator;