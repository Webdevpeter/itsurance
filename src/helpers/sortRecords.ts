import { SortState } from "../components/RecordsTable/types";
import { MergedRecordData } from "../mocks/recordsData";

const sortCompare = (first: string | number, second: string | number, isAscending: boolean): number => {
    if(first < second) { return isAscending ? -1 : 1; }
    if(first > second) { return isAscending ? 1 : -1; }
    return 0;
}

const sortRecords = (records: MergedRecordData[], sortState: SortState): MergedRecordData[] => {
    const { sortBy, isAscending } = sortState;
    return records.sort((firstRecord, secondRecord) => {
        
        switch (sortBy) {
            case 'name':
                return sortCompare(firstRecord.name, secondRecord.name, isAscending);
            case 'phoneNumber':
                return sortCompare(parseInt(firstRecord.phoneNumber, 10) || 0, parseInt(secondRecord.phoneNumber, 10) || 0, isAscending);
            case 'tags':
                return sortCompare(firstRecord.tags.split('.').length, secondRecord.tags.split('.').length, !isAscending);
            case 'favourite':
                return sortCompare(parseInt(firstRecord.is_favorite, 10), parseInt(secondRecord.is_favorite, 10), isAscending);
            default:
                return sortCompare(firstRecord.name, secondRecord.name, isAscending);
        }
    })
}

export default sortRecords;