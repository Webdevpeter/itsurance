import { MergedRecordData } from "../mocks/recordsData";
import { RecordsDataCategory } from "./splitRecordsDataToCategories";

const mergeCategories = (categories: RecordsDataCategory): MergedRecordData[] => {
    return Object.values(categories).reduce((recordsData, categoryRecords) => {
        return [...recordsData, ...categoryRecords];
    }, [] as MergedRecordData[]);
}

export default mergeCategories;