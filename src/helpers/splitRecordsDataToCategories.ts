import { NO_CATEGORY_STRING } from "../constants";
import { MergedRecordData } from "../mocks/recordsData";

const splitRecordsDataToCategories = (records: MergedRecordData[]): RecordsDataCategory => {
    return records.reduce((categories, record) => {
        const category = record.category || NO_CATEGORY_STRING;
        if (!categories[category]) {
            return {
                ...categories,
                [category]: [record]
            }
        }

        return {
            ...categories,
            [category]: [...categories[category], record]
        }
    }, {} as {[key: string]: MergedRecordData[]});
}


export interface RecordsDataCategory {
    [key: string]: MergedRecordData[];
}

export default splitRecordsDataToCategories;