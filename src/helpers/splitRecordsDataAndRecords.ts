import { Record } from "../mocks/records";
import { MergedRecordData, RecordData } from "../mocks/recordsData";

const splitRecordsDataAndRecords = (mergedRecords: MergedRecordData[]): [Record[], RecordData[]] => {
    const records: Record[] = [];
    const recordsData: RecordData[] = mergedRecords.map(record => {
        const { phoneNumber, ...recordData} = record;
        records.push({[record.data_id]: record.phoneNumber});
        
        return {
            ...recordData
        }
    });

    return [records, recordsData];
}

export default splitRecordsDataAndRecords;