import { Record } from "../mocks/records";
import { MergedRecordData, RecordData } from "../mocks/recordsData";

const mergeRecordsWithRecordsData = (records: Record[], recordsData: RecordData[]): MergedRecordData[] => {
    return recordsData.map(recordData => {
        const record = records.find(record => typeof record[recordData.data_id] === 'string');
        return {
            ...recordData,
            phoneNumber: record ? record[recordData.data_id] : ''
        }
    })
}

export default mergeRecordsWithRecordsData;